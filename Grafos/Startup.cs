﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Grafos.Startup))]
namespace Grafos
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
