﻿(function (drawGraph, $, undefined) {


    drawGraph.Graph = null;
    drawGraph.layouter = null;
    drawGraph.renderer = null;
    drawGraph.draw = function () {
        drawGraph.layouter.layout();
        /* draw the graph using the RaphaelJS draw implementation */
        drawGraph.renderer.draw();
    };;

    drawGraph.render = function (r, n) {
        console.log(r);
        console.log(n);
        /* the Raphael set is obligatory, containing all you want to display */
        var set = r.set()
            .push(r.rect(n.point[0] - 30, n.point[1] - 13, 60, 44).attr({ "fill": "#feb", r: "12px", "stroke-width": n.distance == 0 ? "3px" : "1px" }))
            .push(r.text(n.point[0], n.point[1] + 10, (n.label || n.id) + "\n(" + (n.distance == undefined ? "Infinity" : n.distance) + ")"));
        return set;
    };
    drawGraph.init = function (domSelector) {
        
        var width = $('#' + domSelector).width();
        var height =  $('#' + domSelector).height();  

        /* We need to write a new node renderer function to display the computed
           distance.
           (the Raphael graph drawing implementation of Dracula can draw this shape,
           please consult the RaphaelJS reference for details http://raphaeljs.com/) */
        

        drawGraph.Graph = new Graph();
        /* layout the graph using the Spring layout implementation */
        drawGraph.layouter = new Graph.Layout.Spring(drawGraph.Graph);
        drawGraph.renderer = new Graph.Renderer.Raphael(domSelector, drawGraph.Graph, width, height);


        /* modify the edge creation to attach random weights */
        drawGraph.Graph.edgeFactory.build = function (source, target) {
            var e = jQuery.extend(true, {}, this.template);
            e.source = source;
            e.target = target;
            e.style.label = e.weight = Math.floor(Math.random() * 10) + 1;
            return e;
        };

        // creating nodes and passing the new renderer function to overwrite the default one */
        drawGraph.Graph.addNode("New York", { render: drawGraph.render }); // TODO add currying support for nicer code
        drawGraph.Graph.addNode("Berlin", { render: drawGraph.render });
        drawGraph.Graph.addNode("Tel Aviv", { render: drawGraph.render });
        drawGraph.Graph.addNode("Tokyo", { render: drawGraph.render });
        drawGraph.Graph.addNode("Roma", { render: drawGraph.render });
        drawGraph.Graph.addNode("Madrid", { render: drawGraph.render });

        /* connections */
        drawGraph.Graph.addEdge("Tokyo", "Tel Aviv"/*, {weight:9, directed: true, stroke : "#bfa"}*/); // also supports directed graphs, but currently doesn't look that nice
        drawGraph.Graph.addEdge("Tokyo", "New York");
        drawGraph.Graph.addEdge("Tokyo", "Berlin");
        drawGraph.Graph.addEdge("Tel Aviv", "Berlin");
        drawGraph.Graph.addEdge("Tel Aviv", "New York");
        drawGraph.Graph.addEdge("Tel Aviv", "Roma");
        drawGraph.Graph.addEdge("Roma", "New York");
        drawGraph.Graph.addEdge("Berlin", "New York");
        drawGraph.Graph.addEdge("Madrid", "New York");
        drawGraph.Graph.addEdge("Madrid", "Roma");
        drawGraph.Graph.addEdge("Madrid", "Tokyo");      
        
        
        drawGraph.draw();
       

        /*    var pos=0;
            step = function(dir) {
                pos+=dir;
                var renderer = new Graph.Renderer.Raphael('canvas', g.snapshots[pos], width, height);
                renderer.draw();
            };*/
    };

    drawGraph.getShorTestPathBellmanFord = function(node) {
        /* calculating the shortest paths via Bellman Ford */
        bellman_ford(drawGraph.Graph, drawGraph.Graph.nodes[node]);
    };
    drawGraph.getShorTestPathDijkstra = function (node) {
        /* calculating the shortest paths via Dijkstra */
        dijkstra(drawGraph.Graph, drawGraph.Graph.nodes[node]);
    };
    drawGraph.getShorTestPathFloydWarshall = function (node) {
        /* calculating the shortest paths via Floyd-Warshall */
        floyd_warshall(drawGraph.Graph, drawGraph.Graph.nodes[node]);
    };

    drawGraph.addEdge = function (nodeInit, nodeEnd) {
        drawGraph.Graph.addEdge(nodeInit, nodeEnd);

        /* colourising the shortest paths and setting labels */
        for (e in drawGraph.Graph.edges) {
            if (drawGraph.Graph.edges[e].target.predecessor === drawGraph.Graph.edges[e].source || drawGraph.Graph.edges[e].source.predecessor === drawGraph.Graph.edges[e].target) {
                drawGraph.Graph.edges[e].style.stroke = "#bfa";
                drawGraph.Graph.edges[e].style.fill = "#56f";
            } else {
                drawGraph.Graph.edges[e].style.stroke = "#aaa";
            }
        }
    };
    drawGraph.addNode = function(node) {
        drawGraph.Graph.addNode(node, { render: drawGraph.render });
    };

    drawGraph.randomEdgeWeights = function () {
        /* random edge weights (our undirected graph is modelled as a bidirectional graph) */
        for (e in drawGraph.Graph.edges)
            if (drawGraph.Graph.edges[e].backedge != undefined) {
                drawGraph.Graph.edges[e].weight = Math.floor(Math.random() * 10) + 1;
                drawGraph.Graph.edges[e].backedge.weight = drawGraph.Graph.edges[e].weight;
            }        
    };


}(window.drawGraph = window.drawGraph || {}, jQuery));   