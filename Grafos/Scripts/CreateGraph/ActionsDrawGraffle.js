﻿(function (actionDrawGraph, $, undefined) {
    actionDrawGraph.domAddNoteText = '';
    actionDrawGraph.domAddEdgeText = '';
    actionDrawGraph.domAddEdgeTextTwo = '';

    actionDrawGraph.domAddNoteClick = '';
    actionDrawGraph.domAddEdgeClick = '';
    actionDrawGraph.Init = function (obj)
    {
        actionDrawGraph.domAddNoteClick = obj.AddNoteClick;
        actionDrawGraph.domAddNoteText = obj.AddNoteText;

        actionDrawGraph.domAddEdgeText = obj.AddEdgeText;
        actionDrawGraph.domAddEdgeTextTwo = obj.AddEdgeTextTwo;
        actionDrawGraph.domAddEdgeClick = obj.AddEdgeClick;;
    };
    actionDrawGraph.InitControls = function () {

        //AddNote
        $(actionDrawGraph.domAddNoteClick).click(function (e) {
            drawGraph.addNode($(actionDrawGraph.domAddNoteText).val());
            drawGraph.draw();
            $(actionDrawGraph.domAddNoteText).val('');
        });

        //AddEdge
        $(actionDrawGraph.domAddEdgeClick).click(function (e) {
            drawGraph.addEdge($(actionDrawGraph.domAddEdgeText).val(), $(actionDrawGraph.domAddEdgeTextTwo).val());
            drawGraph.draw();
            $(actionDrawGraph.domAddEdgeText).val('');
            $(actionDrawGraph.domAddEdgeTextTwo).val('');
        });
    };

}(window.actionDrawGraph = window.actionDrawGraph || {}, jQuery));