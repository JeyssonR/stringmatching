﻿//   -----------------------------------------------------------------------
//   <copyright file=Program.cs company="Politecnico GranColombiano">
//       Copyright (c) Jeysson Stevens  Ramirez Todos los derechos reservados.
//   </copyright>
//   <author>Jeysson Stevens  Ramirez </author>
//   <Date>  2016 -03-01  - 2:04 p. m.</Date>
//   -----------------------------------------------------------------------

#region

using StringMatching.Generators;

#endregion

namespace StringMatching.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            IGeneratorData generator = new BasicGenerator();

            System.Console.Write(generator.CreateRandomString(1000000, false));
            System.Console.Read();
        }
    }
}