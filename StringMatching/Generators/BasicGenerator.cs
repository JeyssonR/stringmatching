﻿//   -----------------------------------------------------------------------
//   <copyright file=BasicGenerator.cs company="Politecnico GranColombiano">
//       Copyright (c) Jeysson Stevens  Ramirez Todos los derechos reservados.
//   </copyright>
//   <author>Jeysson Stevens  Ramirez </author>
//   <Date>  2016 -03-01  - 2:47 p. m.</Date>
//   -----------------------------------------------------------------------

#region

using System;
using System.Text;

#endregion

namespace StringMatching.Generators
{
    public class BasicGenerator : IGeneratorData
    {
        public string CreateRandomString(int size, bool lowerCase)
        {
            var builder = new StringBuilder();
            var random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26*random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
    }
}