//   -----------------------------------------------------------------------
//   <copyright file=IGeneratorData.cs company="Politecnico GranColombiano">
//       Copyright (c) Jeysson Stevens  Ramirez Todos los derechos reservados.
//   </copyright>
//   <author>Jeysson Stevens  Ramirez </author>
//   <Date>  2016 -03-01  - 2:03 p. m.</Date>
//   -----------------------------------------------------------------------

namespace StringMatching
{
    public interface IGeneratorData
    {
        string CreateRandomString(int size, bool lowerCase);
    }
}