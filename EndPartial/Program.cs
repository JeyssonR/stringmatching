﻿//   -----------------------------------------------------------------------
//   <copyright file=Program.cs company="Banlinea S.A.S">
//       Copyright (c) Banlinea Todos los derechos reservados.
//   </copyright>
//   <author>Jeysson Stevens  Ramirez </author>
//   <Date>  2016 -05-27  - 9:51 PM</Date>
//   <Update> 2016-05-28 - 1:20 PM</Update>
//   -----------------------------------------------------------------------

#region Usings

using System;

#endregion

namespace EndPartial
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var two = new Two();
            var four = new Four();
            var five = new Five();


            Console.WriteLine("----------------------");
            Console.WriteLine("PUNTO 2 entrada: {1,1,1,13,1}");
            var minimunBoxVolumenForCandies = two.MinimunBoxVolumenForCandies(new[] {1, 1, 1, 13, 1});
            Console.WriteLine(minimunBoxVolumenForCandies);

            Console.WriteLine("PUNTO 4 entrada: 10,102");
            var prime = four.CountPalindromicPrime(10, 102);
            Console.WriteLine(prime);

            //Console.WriteLine("PUNTO 5 entrada: 10,102");
            //var a = new[] {"0", "1", "1", "1", "3", "5"};
            //var index = five.Find(a, 0, a.Length - 1, "5");
            //Console.WriteLine(prime);

            Console.ReadLine();
        }
    }

    public class One
    {
    }


    public class Two
    {
        public int MinimunBoxVolumenForCandies(int[] candies)
        {
            var result = 0;
            for (var i = 0; i < candies.Length - 1; i++)
            {
                var a = CalculateVolumen(candies[i]);
                result = result + a;
            }
            result = CalculateVolumen(result);
            return result;
        }

        private static int CalculateVolumen(int number)
        {
            var count = 0;
            var result = 0;
            while (!(result >= number))
            {
                result = (int) Math.Pow(2, count);
                count++;
            }
            return result;
        }
    }

    public class Five
    {
        public int Find(string[] a, int from, int to, string s)
        {
            var interval = to - from;
            var n1 = from + interval/4;
            var n2 = from + interval/4*2;
            var n3 = from + interval/4*3;


            if (to == from)
            {
                if (a[to] == s)
                {
                    return to;
                }
                return -1;
            }
            if (s.CompareTo(a[n1]) == 0)
            {
                return n1;
            }
            if (s.CompareTo(a[n1]) < 0)
            {
                return Find(a, from, n1, s);
            }
            if (s.CompareTo(a[n2]) == 0)
            {
                return n2;
            }
            if (s.CompareTo(a[n2]) < 0)
            {
                return Find(a, n1 + 1, n2, s);
            }
            if (s.CompareTo(a[n3]) == 0)
            {
                return n3;
            }
            if (s.CompareTo(a[n3]) < 0)
            {
                return Find(a, n2 + 1, n3, s);
            }
            return Find(a, n3 + 1, to, s);
        }
    }

    public class Four
    {
        public int CountPalindromicPrime(int a, int b)
        {
            var numberPolindromic = 0;
            while (a <= b)
            {
                if (a.IsPrime())
                {
                    if (a.IsPolindromic())
                    {
                        numberPolindromic = numberPolindromic + 1;
                    }
                }
                a = a + 1;
            }
            return numberPolindromic;
        }
    }

    public static class Extensions
    {
        public static bool IsPrime(this int number)
        {
            var divisible = false;
            for (var i = 2; i < number; i++)
            {
                if (number%i == 0)
                {
                    divisible = true;
                }
            }
            return !divisible;
        }

        public static bool IsPolindromic(this int number)
        {
            var s = number.ToString();
            var end = s.Length/2;
            var counter = 0;

            while (counter < end)
            {
                if (s[counter] != s[s.Length - 1] - counter)
                {
                    return false;
                }
                counter++;
            }

            return true;
        }
    }
}